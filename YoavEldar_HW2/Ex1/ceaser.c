#include "ceaser.h"


char shift_letter(char letter, int offset)
{
	return letter+offset;
}

char * shift_string(char * input, int offset)
{
	int length = sizeof input;
	int i;
	char encrypted[length];

	for (i = 0; i < length; ++i)
		encrypted[i] = shift_letter(input[i],offset);

	return encrypted;
}