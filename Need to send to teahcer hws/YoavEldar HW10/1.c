#include <stdio.h>
#include <stdlib.h>
#include <Windows.h>

int main()
{
	printf("Enter proccess ID: ");
	int x;
	scanf("%d",&x);
	HANDLE proc = OpenProcess(PROCESS_ALL_ACCESS, FALSE, x);
	TerminateProcess(proc, 1);
	printf("Process ended\n");
	system("PAUSE");
	return 0;
}