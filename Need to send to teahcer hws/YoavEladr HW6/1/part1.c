#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <errno.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>

int main()
{
	pid_t h;

	h = fork();

	if(h == 0)
	{
		for(;;)
		{
			printf("I'm still alive!!\n");
		}
	}
	else
	{
		sleep(1);
		printf("disptaching \n");
		kill(h, SIGINT);
		printf("Dispatche\n");
	}
}

