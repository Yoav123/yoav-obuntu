#include <unistd.h>    
#include <sys/types.h>  
#include <errno.h>    
#include <stdio.h>     
#include <sys/wait.h>  
#include <stdlib.h>     
#include <sys/stat.h>
#include <fcntl.h>

int main() 
{
	int pipe;
	pid_t cpid;
	char buf;

    pipe(pipe);


    cpid = fork();

    if (cpid == 0)
	{   
		write(pipe, "magshimim", 9);
	}
}
