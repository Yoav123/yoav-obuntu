#include <unistd.h>
#include <sys/sysinfo.h>
#include <sys/syscall.h>
#include "helper.h"

#define LEN 200

void option1();
void option2();
void option3();

int main(){

	struct fun_desc options[3];
	options[0].name = "option 1 - Exit the program";
	options[0].fun = &option1;
	options[1].name = "option 2 - Getting the user ID"; 
	options[1].fun = &option2;
	options[2].name = "option 3 - Getting the dir of the folder";
	options[2].fun = &option3;

	char i[LEN];
	while(1){
		syscall(SYS_write,0,"Enter your choose: \n1. To exit the program \n2. To print the current user's ID \n3. To print the current folder \n", slen("Enter your choose: \n1. To exit the program \n2. To print the current user's ID \n3. To print the current folder\n"));
		syscall(SYS_read, 0, i, LEN);
		if(i[0] == '1'){
			options[0].fun();
		}
		if(i[0] == '2'){
			options[1].fun();
		}
		if(i[0] == '3'){
			options[2].fun();
		}
		if(!((i[0] == '1') || (i[0] == '2') || (i[0] == '3'))){
			
		}
	}

}

void option1(){
	syscall(SYS_exit,0);
}

void option2(){
	int ID;
	ID = setreuid(geteuid(), getuid());
	syscall(SYS_write, 0, ID , sizeof(ID));
}

void option3(){
	char place[LEN];
	getcwd(place, LEN); 
	syscall(SYS_write, 0, place, slen(place));
}
